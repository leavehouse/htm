# Replicating the simulations from "...A Mathematical Theory of Sparsity"

import random

# `coll` is a collection of elements
# returns a random subset of `coll` with a elements in it
# TODO: don't mutate `coll`?
def random_subset(coll, a):
    if not isinstance(coll, list):
        coll = list(coll)

    assert a <= len(coll), "cannot return {}-length subset of set of size {}".format(a, len(coll))
    subset = set()
    while len(subset) < a:
        i = random.randint(0, len(coll)-1)
        subset.add(coll[i])
        coll.remove(coll[i])
    return subset

def firstn_random_subset(n, a):
    return random_subset([i for i in range(n)], a)


# let [n] := {0, ..., n-1}
# generate a random a-subset of [n], take a random s-subset of that a-subset.
# now generate another a-subsets of [n], and determine if second set theta-matches
# the subsample of the first
def false_positive(n, a, s, theta):
    dendrite = random_subset(firstn_random_subset(n, a), s)
    pattern = firstn_random_subset(n, a)
    return len(dendrite & pattern) >= theta

def false_positive_test(n, a, s, theta, iters):
    pos_count = 0
    for _ in range(iters):
        if false_positive(n, a, s, theta):
            pos_count += 1

    return pos_count

if __name__ == '__main__':
    n = 300
    a = 60
    s = 20
    theta = 10
    print(false_positive_test(n, a, s, theta, 100000))
