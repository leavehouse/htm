import random

# From Materials and Methods section of "Why Neurons Have
# Thousands of Synapses"

col_sparsity = 0.02  # 's' in the HTM spatial pooler paper

segment_thresh = 9 # number of activations needed for a distal segment to
                   # fire, 15 in the papers

lateral_conn_fraction = 0.15 # has no equivalent in the papers. not sure
                             # this is right. WNHTOS notes "we typically
                             # randomly connect between 20 and 40 synapses
                             # on a segment"

perm_thresh = 0.5 # called "connection threshold" in WNHTOS, 0.5 in papers

# used for the Hebbian learning of winning columns
# the following use the convention in COSL, which is that active synapses get
# their permanences increased by p_inc, and inactive synapses get their permanences
# decreased by p_dec
p_inc = 0.1
p_dec = 0.1


def init_distal_segment(M, N, conn_fraction):
    # choose a random subset of cells in the layer to form synapses with
    # for each one, choose a random permanence value
    seg = {}
    for i in range(M):
        for j in range(N):
            if random.random() <= conn_fraction:
                seg[(i,j)] = random.random()
    return seg


class HtmLayer:
    # `S` - distal segments per cell
    # `M` - cells per column
    # `N` - number of columns
    def __init__(self, M, N, S):
        # initialize predictive and active states
        self.P = set()
        self.A = set()
        self.M = M
        self.N = N
        self.S = S

        # initialize distal dendritic segments
        self.D = [[[init_distal_segment(M, N, lateral_conn_fraction)
                    for _ in range(S)]
                   for _ in range(N)]
                  for _ in range(M)]

    # update a segment by increasing the permanences for synapses with
    # presynaptic activity and decreasing the permanences for others
    #
    # `act` is the calculated next activation (A^t)
    # `inc`/`dec` are how much to increment/decrement active/inactive synapses
    # on a segment that fired
    #
    # Note: this uses the convention in COSL which is slightly different from
    # that of WNHTOS, which is to increment active synapses and decrement
    # inactive synapses, rather than decrementing ALL synapses and incrementing
    # the active ones with an amount greater than the decrement amount
    def update_segment(self, i, j, d, act, inc, dec):
        def between_0_1(x):
            return min(max(x, 0), 1)

        for (a, b) in self.D[i][j][d].keys():
            if (a, b) in act:
                self.D[i][j][d][(a, b)] = between_0_1(self.D[i][j][d][(a,b)] + inc)
            else:
                self.D[i][j][d][(a, b)] = between_0_1(self.D[i][j][d][(a,b)] - dec)


    # takes a set of winning column indices, W
    # calculates new active and predictive states, update permanence values
    def step(self, W):
        # compute next "active state" A from W and P
        A = set()
        for j in W:
            if len([i for i in range(self.M) if (i, j) in self.P]) == 0:
                for i in range(self.M):
                    A.add((i, j))
                continue

            for i in range(self.M):
                if (i,j) in self.P:
                    A.add((i, j))

        # compute next "predictive state" from A
        P = set()
        for i in range(self.M):
            for j in range(self.N):
                for d in range(self.S):
                    D_tilde = self.segment_connected_synapses(i, j, d, perm_thresh)

                    if len(self.A & D_tilde) > segment_thresh:
                        P.add((i,j))
                        break

        # update permanences from previous step
        # TODO: decay
        for j in W:
            # if there were predicted cells in this column, update the
            # relevant segments. otherwise, update the segment that was
            # closest to firing
            predicted = [a for (a, b) in P if b == j]
            if len(predicted) > 0:
                for i in predicted:
                    # get every segment that was above segment_thresh
                    for d in range(self.S):
                        D_tilde = self.segment_connected_synapses(i, j, d, perm_thresh)
                        if len(A & D_tilde) > segment_thresh:
                            self.update_segment(i, j, d, A, p_inc, p_dec)
            else:
                # find the segments that were closest to firing in j
                max_active = -1
                for i in range(self.M):
                    for d in range(self.S):
                        num_active = self.count_segment_preactivity(i, j, d, A)
                        if num_active > max_active:
                            max_i_d = set([(i, d)])
                            max_active = num_active
                        elif num_active == max_active:
                            max_i_d.add((i, d))

                for (i, d) in max_i_d:
                    self.update_segment(i, j, d, A, p_inc, p_dec)

        # update predictive and active states
        self.A = A
        self.P = P


    # counts the overlap between potential synapses (those with nonzero permanence)
    # and the input `A`
    def count_segment_preactivity(self, i, j, d, A):
        return len(set(self.D[i][j][d].keys()) & A)

    # calculate the set of connected synapses on a segment
    def segment_connected_synapses(self, i, j, d, conn_thresh):
        return {k for (k, p) in self.D[i][j][d].items() if p >= conn_thresh}

    def print_cell_states(self):
        print("A\n==========")
        print(self.A)
        print("\nP\n==========")
        print(self.P)


if __name__ == '__main__':
    n = 8
    N = n**2  # number of columns, 2048 in the papers
    M = 4     # number of cells in each column, 32 in the papers

    num_distal_segs = 20  # number of distal dendritic segments
                          # this is 128 in the papers


    htm = HtmLayer(M, N, num_distal_segs)

    # define the input layer
    # TODO: implement a scalar encoder of some kind?
    input_len = 100
    # generate a sequence of inputs
    inputs = [{i for i in range(input_len) if random.random() <= 0.3}
              for _ in range(4)]

    for inp in inputs:
        print("W: ", W)
        print()
        htm.step(W)
        htm.print_cell_states()
