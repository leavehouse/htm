import math
import tkinter as tk
from tkinter import ttk

CANVAS_BG_COLOR = 'white'

def draw_grid_element(canvas, tl, size, tags=None):
    cid = canvas.create_rectangle(tl, tl[0] + size + 1, tl[1] + size + 1,
                                  fill=CANVAS_BG_COLOR, tags=tags)
    return cid

def draw_grid(canvas, grid_rows, grid_cols, top_left_pos, size, spacing, tags=None):
    for i in range(grid_rows):
        for j in range(grid_cols):
            tl = (top_left_pos[0] + j * (size + spacing),
                  top_left_pos[1] + i * (size + spacing))
            draw_grid_element(canvas, tl, size, tags=tags)

def config_grid_canvas_size(canvas, top_left_pos, r):
    bbox = canvas.bbox(tk.ALL)
    canvas_w = top_left_pos[0] * 2 + (bbox[2] - bbox[0])
    canvas_h = top_left_pos[1] * 2 + (bbox[3] - bbox[1])
    canvas.configure(width = canvas_w, height = canvas_h)

# assumes that the grid of circles is created first in the canvas
def fill_circles(canvas, cids, fill_color='#4a90d9', tag='active'):
    # clean up previously activated circles
    clear_filled_circles(canvas, tag)

    for cid in cids:
        fill_and_tag_circle(canvas, cid, fill_color, tag)

def fill_and_tag_circle(canvas, cid, fill_color, tag):
    canvas.itemconfigure(cid, fill=fill_color)
    canvas.addtag(tag, 'withtag', cid)

def clear_filled_circles(canvas, tag='active'):
    for cid in canvas.find_withtag(tag):
        canvas.itemconfigure(cid, fill=CANVAS_BG_COLOR)
    canvas.dtag(tag, tag)

# grid coords start at zero (i.e. for an MxN grid, they range from (0,0)
# to (M-1, N-1))
def sp_grid_coords_to_ids(coords, per_row):
    new_coords = set()
    for coord in coords:
        new_coords.add(sp_grid_coord_to_id(coord, per_row))

    return new_coords

def sp_grid_coord_to_id(coord, per_row):
    (row, col) = coord
    return per_row*row + col + 1


def id_to_sp_grid_coord(cid, per_row):
    cid -= 1
    return (cid // per_row, cid % per_row)

# TODO: add 0.5 and use rounding instead?
def perm_to_hex_color(perm):
    perm = 1 - perm
    if perm == 0.:
        h =  0
    else:
        h = math.ceil(perm * 256) - 1

    hex_str = "%0.2x" % h
    return "#" + hex_str + hex_str + hex_str



class SPUI:
    DEFAULT_GRID_ELE_SIZE=16
    DEFAULT_SPACING=5
    def __init__(self, sp, gen_input_func, grid_ele_size=None, spacing=None):
        self.sp = sp

        self.root = tk.Tk()
        self.content = tk.Frame(self.root)
        self.canvas_input = tk.Canvas(self.content, bg=CANVAS_BG_COLOR)
        self.canvas_columns = tk.Canvas(self.content, bg="#ffffff")
        self.new_input = tk.Button(self.content, text="Generate new input")

        if grid_ele_size is None:
            grid_ele_size = SPUI.DEFAULT_GRID_ELE_SIZE

        if spacing is None:
            spacing = SPUI.DEFAULT_SPACING

        tl_center = (20, 20)

        self.grid_ele_size = grid_ele_size
        self.spacing = spacing

        draw_grid(self.canvas_input, self.sp.input_dims[0], self.sp.input_dims[1],
                  tl_center, self.grid_ele_size, self.spacing, tags='unit')

        input_grid_ycoord = self.canvas_input.bbox('unit')[3]
        perm_tl_coord = (20, input_grid_ycoord + 50)
        draw_grid(self.canvas_input, self.sp.input_dims[0], self.sp.input_dims[1],
                  perm_tl_coord, self.grid_ele_size, self.spacing, tags='perm_unit')
        self.hide_perm_grid()

        self.inspected_column = None

        draw_grid(self.canvas_columns, self.sp.column_dims[0], self.sp.column_dims[1],
                  tl_center, self.grid_ele_size, self.spacing, tags='unit')


        config_grid_canvas_size(self.canvas_input, tl_center, self.grid_ele_size)
        config_grid_canvas_size(self.canvas_columns, tl_center, self.grid_ele_size)

        def gen_compute_display(event):
            z = gen_input_func()
            cols = self.sp.compute(z, learn=True)
            self.display_input_columns(z, cols)
            if self.inspected_column is not None: self.display_permanences()

        def column_click(event):
            # don't need to use canvasx/canvasy because (currently)
            # we never scroll the canvases
            col_id = event.widget.find_closest(event.x, event.y)[0]
            self.handle_column_click(col_id)

        self.new_input.bind('<Button-1>', gen_compute_display)
        self.canvas_columns.tag_bind(tk.ALL, '<Button-1>', column_click)

        self.content.grid(column=0, row=0)
        self.new_input.grid(column=0, row=0, sticky=(tk.W, tk.E))
        self.canvas_input.grid(column=0, row=1, sticky=(tk.N, tk.S, tk.W, tk.E))
        self.canvas_columns.grid(column=1, row=1, sticky=(tk.N, tk.S, tk.W, tk.E))

        self.root.mainloop()

    def handle_column_click(self, col_id):
        if self.inspected_column is not None:
            self.canvas_columns.itemconfigure(self.inspected_column, width=1)

        if self.inspected_column is None or self.inspected_column != col_id:
            self.inspected_column = col_id
            self.canvas_columns.itemconfigure(col_id, width = 3)

            # prepare permanence grid
            if self.perm_grid_state == 'hidden': self.show_perm_grid()
            clear_filled_circles(self.canvas_input, tag='perm_perm')
            self.canvas_input.itemconfigure('perm_unit', outline='#cccccc')

            self.display_permanences()

        else:
            self.inspected_column = None
            self.hide_perm_grid()

    def hide_perm_grid(self):
        self.canvas_input.itemconfigure('perm_unit', state='hidden')
        self.perm_grid_state = 'hidden'

    def show_perm_grid(self):
        self.canvas_input.itemconfigure('perm_unit', state='normal')
        self.perm_grid_state = 'visible'

    def display_permanences(self):
        grid_coord = id_to_sp_grid_coord(self.inspected_column, self.sp.column_dims[1])

        for i, perm in self.sp.perms[grid_coord].items():
            fill_color = perm_to_hex_color(perm)
            cid = sp_grid_coord_to_id(i, self.sp.input_dims[1]) + self.sp.num_inputs
            self.canvas_input.itemconfigure(cid, outline='black')
            fill_and_tag_circle(self.canvas_input, cid,
                                fill_color=fill_color, tag='perm_perm')


    # display input and columns
    def display_input_columns(self, inps, cols):
        fill_circles(self.canvas_input,
                         sp_grid_coords_to_ids(inps, self.sp.input_dims[1]))
        fill_circles(self.canvas_columns,
                         sp_grid_coords_to_ids(cols, self.sp.column_dims[1]))
