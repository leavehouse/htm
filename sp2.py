import numpy as np

import itertools
import math
import random

# fraction of potential receptive field a cell is connected to
# TODO: why was this set to 0.5?
RECEPTIVE_FIELD_FRACTION = 0.3

# TODO: this has to depend on dimensions of input and SP regions,
# so i dont think it can be a constant even though it seems to be one
# in the paper
RECEPTIVE_FIELD_RADIUS = 5

# theta_stim = 1 is used in SP paper
OVERLAP_THRESHOLD = 1

# theta_c = 0.5 is used in SP paper
CONN_THRESHOLD = 0.5

P_INC = 0.1
P_DEC = 0.02

def print_overlap(ol, columns_per_row):
    i = 0
    while i < len(ol):
        row_display = str(ol[i])
        num_to_print = min(columns_per_row, len(ol)-i)
        for j in range(1, num_to_print):
            row_display += ", " + str(ol[i+j])

        print(row_display)
        i += num_to_print

# TODO: implement boosting, local receptive fields
class SpatialPooler:
    # TODO: support arbitary input dimension?
    @staticmethod
    def init_receptive_field(input_dims, center):
        perms = {}
        # hypercube 
        for i in potential_potential_field:
            if random.random() < RECEPTIVE_FIELD_FRACTION:
                perms[i] = random.random()
        return perms

    # TODO: support arbitrary input dimension?
    def __init__(self, input_dims, column_dims, sparsity, inhibition_radius, do_learn=False):
        self.input_dims = input_dims
        self.column_dims = column_dims
        self.sparsity = sparsity
        self.inhibition_radius = inhibition_radius
        self.do_learn = do_learn

        # permanence table for the (shared) proximal synapses of each column
        self.P = { i: self.random_receptive_field(input_len)
                   for i in self.column_indices() }

    def column_indices(self):
        rs = [range(d) for d in self.column_dims]
        return itertools.product(*rs)

    # Returns subset of i's receptive field that is connected (i.e. that has
    # permanence values exceeding the connection threshold
    def connection(self, i):
        return {k for (k, perm) in self.P[i].items() if perm >= CONN_THRESHOLD}

    # TODO: add non-global inhibition radii
    def neighbors(self, i):
        assert self.inhibition_radius >= 0, "inhibition_radius is negative"

        if math.isinf(self.inhibition_radius):
            return self.column_indices()
        else:
            raise NotImplementedError
            #return [j for j in range(self.num_columns)
            #          if self.column_distance(i, j) < self.inhibition_radius]

    def column_distance(self, i, j):
        return abs(j - i)

    # spatially pool input. result is a set of winning columns
    def sp(self, z):
        #  compute input overlaps o_i(z) for every column i
        # TODO: add boosting
        o = [len(z & self.connection(i)) for i in range(self.num_columns)]

        print_overlap(o, 10)

        # NOTE: I feel like this should return a set for consistency, but
        # this is only used to feed into numpy.percentile, which can't deal
        # with the set for some reason, so it's nicer if it's a list
        def neighbor_overlap(i):
            return [o[i] for i in self.neighbors(i)]

        def is_activated(i):
            return (o[i] > OVERLAP_THRESHOLD
                    and o[i] >= np.percentile(neighbor_overlap(i),
                                              100 * (1 - self.sparsity)))

        # adjust synaptic permanences via Hebbian-like rule
        def learn():
            for j in range(self.input_len):
                if j in z:
                    self.P[j] = min(1, self.P[j] + P_INC)
                else:
                    self.P[j] = max(0, self.P[j] + P_DEC)
            
        activated = set()
        for i in range(self.num_columns):
            if is_activated(i):
                activated.add(i)
                if self.do_learn:
                    learn()
        return activated


def binary_vec_to_set(v):
    return {i for i in range(len(v)) if v[i] == 1}

def test_sparsities(num_inputs, column_dims, target_sparsity, num_trials, z):
    counts = {}
    for _ in range(num_trials):
        sp = SpatialPooler(num_inputs, column_dims, target_sparsity)
        sdr = sp.sp(z)

        num_active = len(sdr)

        if num_active not in counts:
            counts[num_active] = 0

        counts[num_active] += 1
    return counts

def print_sdr(sdr, num_columns, columns_per_row):
    # to print a row, there's a leading |, then a sequence of cells, _| or #|
    # (so that, e.g. |_|_|#|_|_|_|_|)
    i = 0
    while i < num_columns:
        row_display = "|"
        num_to_print = min(columns_per_row, num_columns-i)
        for j in range(num_to_print):
            if (i+j) in sdr:
                row_display += "#|"
            else:
                row_display += "_|"

        print(row_display)
        i += num_to_print

if __name__ == '__main__':
    SPARSITY = 0.02
    INPUT_LEN = 16
    COLUMN_DIMS = [10, 10]
    z = [1 if random.random() > 0.5 else 0 for _ in range(INPUT_LEN)]
    print(z)
    z = binary_vec_to_set(z)

    #counts = test_sparsities(INPUT_LEN, [50], SPARSITY, 1000, z)
    #print(sorted(counts.items()))

    # print sdr instead
    sp = SpatialPooler(INPUT_LEN, COLUMN_DIMS, SPARSITY, 5)
    sdr = sp.sp(z)
    print_sdr(sdr, COLUMN_DIMS[0] * COLUMN_DIMS[1], 10)
