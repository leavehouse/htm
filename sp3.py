import itertools
import math
import random
import turtle
from typing import List, Set, Tuple
# Porting from nupic.core's SpatialPooler.cpp

import spui

Index = Tuple[int, ...]

# Let I = num_inputs, C = num_columns
# First we consider the closed interval [0, I] of the real line. Place I dots
# on this interval at [0.5, 1.5, 2.5, ..., (I - 1) + 0.5]. These correspond to
# the I input units [0, 1, 2, ..., I-1]. Now divide [0, I] into C equal segments.
# For each segment, pick the input unit whose coordinate is closest to the center
# of the segment. This is the same as taking the floor of the center of the
# segment. The k-th interval center is
#
#  (kM + (k+1)M)/2 = (2k + 1)M/2 = (k + 0.5)*M
#
# where M := I/C
def map_center_1d(num_columns: int, num_inputs: int, col: int) -> int:
    assert col < num_columns
    return math.floor((col + 0.5) * num_inputs / num_columns)


# TODO: "trimming", which is we set a small threshold, and any permanences
# below that get "trimmed" to 0. something to do with making potential pools
# more sparse?
class SpatialPooler:
    def __init__(self,
                 input_dims: List[int],
                 column_dims: List[int],
                 pot_radius: int,
                 pot_pct: float = 0.5,
                 conn_threshold: int = 0.1,
                 global_inhib: bool = True,
                 inhib_area_density: float = 0.02,
                 stim_threshold: int = 0,
                 perm_dec: float = 0.01,
                 perm_inc: float = 0.1,
                 duty_cycle_period: int = 1000,
                 boost_strength: float = 0.0):

        assert len(input_dims) == len(column_dims), \
                "input and column spaces have different dimensions"
        self.D = len(input_dims)
        num_inputs = 1
        num_columns = 1
        for i in range(self.D):
            num_inputs *= input_dims[i]
            num_columns *= column_dims[i]

        assert num_inputs > 0, "number of inputs is zero"
        assert num_columns > 0, "number of columns is zero"
        assert (pot_pct > 0 and pot_pct <= 1)
        # TODO: in nupic, this assert checks that inhib_area_density <= 0.5. Why?
        assert (inhib_area_density > 0 and inhib_area_density <= 1)

        self.input_dims = input_dims
        self.column_dims = column_dims
        self.pot_radius = pot_radius
        self.pot_pct = pot_pct
        self.conn_threshold = conn_threshold
        self.global_inhib = global_inhib
        self.inhib_area_density = inhib_area_density
        self.stim_threshold = stim_threshold
        self.perm_dec = perm_dec
        self.perm_inc = perm_inc
        self.duty_cycle_period = duty_cycle_period
        self.boost_strength = boost_strength

        self.iteration_count = 0
        self.num_inputs = num_inputs
        self.num_columns = num_columns

        # initialize potential pools, permanences, boost factors and duty cycles for each column
        init_connected_frac = 0.5
        self.potential_pool = {}
        self.perms = {}
        self.connected_pool = {}
        self.boost = {}
        self.active_duty_cycle = {}
        for ci in self.column_indices():
            self.potential_pool[ci] = self.init_potential_pool(ci)
            self.perms[ci] = self.init_pool_permanences(ci, init_connected_frac)
            self.update_connected_pool(ci)
            self.boost[ci] = 1.
            self.active_duty_cycle[ci] = 0

        self.update_inhib_radius()

    def update_connected_pool(self, col: Index):
        self.connected_pool[col] = { inp for inp, perm in self.perms[col].items()
                                     if perm >= self.conn_threshold }


    def compute(self, z: Set[Index], learn=False) -> Set[Index]:
        self.iteration_count += 1

        overlap = {i: len(z & self.connected_pool[i])
                   for i in self.column_indices()}

        boosted_overlap = {i: self.boost[i] * ol for i, ol in overlap.items()}

        if self.inhib_radius is None:
            # global inhibition
            active = self.get_active_global(boosted_overlap)
        else:
            active = self.get_active_local(boosted_overlap)

        if learn:
            # Update permanences via Hebbian-like learning
            for col in self.column_indices():
                for inp in self.potential_pool[col]:
                    if inp in active:
                        self.perms[col][inp] = min(self.perms[col][inp] + self.perm_inc, 1)
                    else:
                        self.perms[col][inp] = max(self.perms[col][inp] - self.perm_dec, 0)
                self.update_connected_pool(col)

            self.update_active_duty_cycles(active)
            self.update_boost_factors()

            self.update_inhib_radius()

        return active


    def get_active_global(self, overlap: Set[int]) -> Set[Index]:
        # TODO: look at tiebreaking. i think ties go to most recently added
        def potentially_active(col: Index,
                               pot_active: List[Tuple[Index, int]],
                               num_active_target: int):
            return (overlap[col] >= self.stim_threshold
                    and (len(pot_active) < num_active_target
                         or overlap[col] > pot_active[num_active_target - 1][1]))

        def add_to_pot_active(col: Index,
                              pot_active: List[Tuple[Index, int]]):
            for i in range(len(pot_active)):
                if overlap[col] >= pot_active[i][1]:
                    pot_active.insert(i, (col, overlap[col]))
                    return
            pot_active.append((col, overlap[col]))

        num_active_target = round(self.inhib_area_density * self.num_columns)
        pot_active = []
        for ci in self.column_indices():
            if potentially_active(ci, pot_active, num_active_target):
                add_to_pot_active(ci, pot_active)

        num_active = min(num_active_target, len(pot_active))
        return {i[0] for i in pot_active[:num_active]}

    def get_active_local(self, overlap: Set[int]) -> Set[Index]:
        # TODO: tiebreaking. ties go to first added.
        active = set()
        for col in self.column_indices():
            if overlap[col] >= self.stim_threshold:
                num_neighbors = 0
                num_bigger = 0
                for neighbor in Neighborhood(col, self.inhib_radius, self.column_dims):
                    if neighbor != col:
                        num_neighbors += 1
                        diff = overlap[neighbor] - overlap[col]
                        if diff > 0 or (diff == 0 and neighbor in active):
                            num_bigger += 1
                num_active = round(self.inhib_area_density * (num_neighbors + 1))
                if num_bigger < num_active:
                    active.add(col)
        return active

    def update_active_duty_cycles(self, active):
        period = min(self.iteration_count, self.duty_cycle_period)
        assert period > 0
        for col in self.column_indices():
            activation = 1 if col in active else 0
            self.active_duty_cycle[col] = ((period - 1) * self.active_duty_cycle[col] + activation) / period

    # TODO: implement boosting for local inhibition
    def update_boost_factors(self):
        assert self.global_inhib

        for col in self.column_indices():
            self.boost[col] = math.exp(self.boost_strength * (self.inhib_area_density - self.active_duty_cycle[col]))

    def column_indices(self):
        column_dim_ranges = [range(x) for x in self.column_dims]
        return itertools.product(*column_dim_ranges)

    def column_input_center(self, column: Index) -> Index:
        center = []
        for  (C, I, col) in zip(self.column_dims, self.input_dims, column):
            center.append(map_center_1d(C, I, col))
        return center

    def init_potential_pool(self, column: Index) -> Set[Index]:
        input_center = self.column_input_center(column)
        nhood = [n for n in Neighborhood(input_center, self.pot_radius, self.input_dims)]
        pool_size = round(len(nhood) * self.pot_pct)
        return set(random.sample(nhood, pool_size))

    def init_pool_permanences(self, column: Index, conn_frac: float):
        perms = {}
        for inp in self.potential_pool[column]:
            if random.random() <= conn_frac:
                perms[inp] = (self.conn_threshold
                              + (1 - self.conn_threshold) * random.random())
            else:
                perms[inp] = self.conn_threshold * random.random()
        return perms

    def update_inhib_radius(self):
        if self.global_inhib:
            self.inhib_radius = None
        else:
            avg_rf_diam = 0
            for ci in self.column_indices():
                avg_rf_diam += self.avg_receptive_field_diam(ci)
            avg_rf_diam /= self.num_columns
            diameter = avg_rf_diam * self.avg_columns_per_input()
            radius = (diameter - 1)/2
            self.inhib_radius = max(round(radius), 1)

    # calculate the average diameter of all the dimensions of the receptive
    # field for `column`. The *receptive field* is here considered the
    # connected pool
    def avg_receptive_field_diam(self, column: Index) -> float:
        diam = 0
        for i in range(self.D):
            field_begin = self.input_dims[i]
            field_end = 0
            for inp in self.connected_pool[column]:
                if inp[i] < field_begin:
                    field_begin = inp[i]
                if inp[i] > field_end:
                    field_end = inp[i]

            diam += field_end - field_begin + 1
        return diam/self.D

    def avg_columns_per_input(self):
        columns_per_input = 0
        for i in range(self.D):
            columns_per_input += self.column_dims[i] / self.input_dims[i]
        return columns_per_input / self.D


class Neighborhood:
    def __init__(self, center: Index, radius: int, dims: List[int]):
        assert len(center) == len(dims), \
                "Dimension mismatch between center coordinates and space"
        self.center = center
        self.radius = radius
        self.dims = dims
        self.index = []
        self.nhood_min = []
        self.nhood_max = []
        for i in range(len(dims)):
            self.nhood_min.append(max(center[i] - radius, 0))
            self.nhood_max.append(min(center[i] + radius, dims[i] - 1))
            self.index.append(self.nhood_min[i])

    def __iter__(self):
        return self

    def __next__(self):
        if self.index is None:
            raise StopIteration()
        else:
            coord = self.index[:]
            # find the first dimension smaller than nhood_max
            # increment it, set all preceding dimensions to nhood_min
            first_incable = None
            for i in range(len(self.dims) - 1, -1, -1):
                if self.index[i] < self.nhood_max[i]:
                    first_incable = i
                    break

            if first_incable is None:
                self.index = None
            else:
                self.index[first_incable] += 1
                for i in range(first_incable+1, len(self.dims)):
                    self.index[i] = self.nhood_min[i]
            return tuple(coord)

def gen_input_bar(start, bar_type: str, length: int) -> Set[Index]:
    assert len(start) == 2
    inp = set()
    for i in range(length):
        if bar_type == 'horizontal':
            inp.add((start[0], start[1] + i))
        else:
            inp.add((start[0] + i, start[1]))

    return inp

def gen_input_h_v(grid_rows, grid_cols, length: int) -> Set[Index]:
    # pick a random starting point
    h_start_row = random.randint(0, grid_rows - 1)
    h_start_col = random.randint(0, grid_cols - length - 1)

    v_start_row = random.randint(0, grid_rows - length - 1)
    v_start_col = random.randint(0, grid_cols - 1)

    h_bar = gen_input_bar((h_start_row, h_start_col), 'horizontal', length)
    v_bar = gen_input_bar((v_start_row, v_start_col), 'vertical', length)

    return h_bar | v_bar

def make_gen_input(sp):
    def gen_input():
        return gen_input_h_v(sp.input_dims[0], sp.input_dims[1], 5)

    return gen_input


if __name__ == '__main__':
    (in_rows, in_cols) = (10, 10)
    (out_rows, out_cols) = (24, 24)
    sp = SpatialPooler([in_rows, in_cols], [out_rows, out_cols], 3, global_inhib=True, boost_strength=100)
    spui = spui.SPUI(sp, make_gen_input(sp))

