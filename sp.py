import numpy as np

import random

# needed to model in spatial pooler:
#
#   - rectangular array of mini-columns
#   - space (rectangular array?) of inputs
#   - the set of potential synapses for each mini-column
#   - the permanence value for each potential synapse
#   - connection matrix (derived from permanence values and the connection 
#     threshold)
#   - the process of activating columns from a given input (winner take all
#     using target activation density)
#   - Hebbian rule for changing synaptic permanences for active neurons


# minicolumns in an SP region are organized into a (rm x rn) array
rm, rn = (5, 5)

# for simplicity, input is 1-dimensional.
# TODO: this will require hacking the radius stuff?
N = 16

# in the HTM SP paper they set this to 0.5
CONN_THRESH = 0.5

# fraction of inputs in a mini-column's radius that the mini-column forms
# potential synapses with
FRAC_POTENTIAL_SYN = 0.75

# boost is initialized to 1 for each minicolumn
boost = np.ones((rm, rn))

# TODO: implement input radii (currently the input radius is infinite)
# for the (i,j)-th mini-column, potential_synapses[(i,j)] is a dict where
# the keys are the indices of potential synapses and the values are the synapse permanences
potential_synapses = {}
for i in range(rn):
    for j in range(rm):
        potential_synapses[(i, j)] = {}
        for k in range(N):
            if random.random() <= FRAC_POTENTIAL_SYN:
                potential_synapses[(i,j)][k] = random.random()


synapse_conns = np.zeros(rm, rn, N)
for i in range(rm):
    for j in range(rn):
        # for each potential synapse, if the permanence exceeds CONN_THRESH, flip it to 1
        for k in potential_synapses[(i,j)].keys():
            if potential_synapses[(i,j)][k] >= CONN_THRESH:
                synapse_conns[i,j,k] = 1


# `z` is assumed to be an `N`-dimensional column vector
def calc_overlap(z, index, W):
    return boost[index] * np.dot(W[index].reshape(1, N), z)



if __name__ == '__main__':
    z = np.array([0,1,1,0,
                  1,0,0,1,
                  0,0,0,0,
                  1,1,1,1]).reshape(N,1)

    print(calc_overlap(z, (1,2), synapse_conns))
